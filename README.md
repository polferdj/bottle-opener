# Magnetic Bottle Opener
* Similar to a dropcatch (https://dropandcatch.com/)
* Magnetic so you don't have to mount to a wall
* Magnetic so it mounts directly to your fridge

# Design
* Included in this repo is a sketchup file of my design

# Parts
put parts lists and links here